# Introduction

Alas! Sparkify is growing! Their user base and song database have grown substantially and they now want to move their data warehouse to a data lake. As the company's data engineer, I will be responsible for developing a data lake and migrating the data from the existing data warehouse. Currently, the data resides in an S3 bucket, within a directory of JSON logs as well as a directory with JSON metadata for the songs that are streaming. 

## Project Description

Here, I will apply the knowledge learned in the Data Lake section of the course to build an ETL pipeline for a Data Lake hosted on Amazon S3. The pipeline extracts data from S3, processes it using Spark, then loads it back into S3 into new tables. This work will allow the analytics team to query for insights on the user's behavior.

## Description of Data

The Sparkify database consists of five tables in the star schema shown below. The fact table is called  `songplays`. This table contains a record of each song played on its music streaming app. In addition to the fact table, there are four dimension tables: `artists`, `songs`, `time`, and `users`

![Star Schema](redshift-star-schema.png)


## Explanation of Files

  

**Project Template**

This project includes three files:

**1. etl.py -** Reads the data from S3, processes it using Spark, then writes the data back to S3.

**2. dl.cfg -** Contains AWS Credentials.

**3. README.md -** An explanation of the project.