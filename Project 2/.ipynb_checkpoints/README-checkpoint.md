# Project 3 Data Warehousing

## Summary

Sparkify, a fictional music streaming startup has grown their user base and song database and as a result, want to move their processes and data onto the cloud. Currently, their data resides in S3, in a directory of JSON logs on user activity on the app, as well as a directory with JSON metadata on the songs in their app.

In this project, we'll apply what was learned about cloud data warehouses to build an ETL pipeline for a database hosted on AWS's fully managed data warehousing service Redshift. We will load data from S3 to staging tables on Redshift and execute SQL statements that create the analytics tables from these staging tables.

## Description of Data

The Sparkify database consists of five tables in the star schema shown below. The fact table is called  `songplays`. This table contains a record of each song played on its music streaming app. In addition to the fact table, there are four dimension tables: `artists`, `songs`, `time`, and `users`

![Star Schema](redshift-star-schema.png)

## Explanation of Files

This project consists of four main files and this file which serves as documentation. 

 - `dwh.cfg` - a configuration file with login credentials for an active AWS Redshift cluster and ARN for an IAM role with S3 read access.
 - `create_tables.py` - a Python script that creates and drops new data tables.
 - `etl.py` - a Python script that loads data from S3 into staging tables on Redshift and then processes that data into analytics tables.
 - `sql_queries.py` - A Python script that defines SQL statements for table creation.


## Running the Python Scripts
To run this project, do the following
1.  Activate an AWS Redshift cluster and create an IAM role with S3 read access. 
2. Enter AWS credentials into the configuration file.
3. Run `sql_queries.py` from terminal or python console to load table create and insert queries.
4.  Run `create_tables.py` from terminal or python console to create staging and analytical tables.
5.  Run `etl.py` from terminal or python console to process and load data into the data warehouse.